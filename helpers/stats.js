const mongoose = require('mongoose')

const Image = mongoose.model('Image')
const Comment = mongoose.model('Comment')



module.exports = () =>
    Promise.all([
      Image.aggregate([{
          $group: {
              _id: 'total',
              images: {$sum: 1},
              views: {$sum: '$views'},
              likes: {$sum: '$likes'}
          }
      }]),
      Comment.count()
    ])
    .then(([[docs],num]) => ({
        images: docs ? docs.images : 0,
        views: docs ? docs.views : 0,
        likes: docs ? docs.likes : 0,
        comments: num
    }))
