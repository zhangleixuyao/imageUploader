const mongoose = require('mongoose')

const Image = mongoose.model('Image')

module.exports = () =>
        Image.find().sort({likes: -1}).limit(9)
