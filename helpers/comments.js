const mongoose = require('mongoose')

const Comment = mongoose.model('Comment')
const Image = mongoose.model('Image')


module.exports = () =>
  Comment.find().sort({timestamp: -1}).limit(5).populate('image_id')
