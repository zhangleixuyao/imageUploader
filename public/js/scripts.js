$(function(){
    $('#post-comment').hide();
    $('#btn-comment').on('click', function(event) {
        event.preventDefault();

        $('#post-comment').slideDown();
    });


    $('#btn-like').on('click', function(event) {
        event.preventDefault();

        var imgId = $(this).data('id');

        $.post('/images/' + imgId + '/like').done(function(data) {
            $('.likes-count').text(data.likes);
            var likes = $('.allLikes').html();
            $('.allLikes').text(Number(likes)+1);
        });
    });


    $('#btn-delete').on('click', function(event) {
        event.preventDefault();

        var $this = $(this);

        var remove = confirm('你确定要删除这张照片吗?');
        if (remove) {

            var imgId = $(this).data('id');

            $.ajax({
                url: '/images/' + imgId,
                type: 'DELETE'
            }).done(function(result) {
                if (result.res) {
                    $('#btn-like').attr("disabled",true);
                    $('#post-comment').hide();
                    $this.removeClass('btn-danger').addClass('btn-success');
                    $this.find('i').removeClass('fa-times').addClass('fa-check');
                    $this.append('<span> Deleted!</span>');
                    $('#btn-delete').attr("disabled",true);

                    setTimeout("window.location.href='http://127.0.0.1:3000/'",1000);
                } else {
                  alert("对不起,您没有权限删除此照片!")
                }
            });
        }
    });

    timeago().render(document.querySelectorAll('.need_to_be_rendered'),'zh_CN');

    var name = "authorization="
    var allcookies = document.cookie;
    var cookie_pos = allcookies.indexOf(name);
    if (cookie_pos != -1) {
      $('#user-register').hide();
      $('#user-login').hide();
    } else {
      $('#user-logout').hide();
    }
});
