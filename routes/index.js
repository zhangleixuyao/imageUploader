const router = require('koa-router')()
const mongoose = require('mongoose')
const Image = mongoose.model('Image')
const stats = require('../helpers/stats')
const popular = require('../helpers/popular')
const comments = require('../helpers/comments')

router.get('/', async (ctx, next) => {

      await Promise.all([
          Image.find().sort({timestamp: -1}).limit(12),
          stats(),
          popular(),
          comments()
      ])
      .then(([images, stats, pImages, comments]) => ctx.render('index',
          {images, stats, pImages, comments}
      ))
      .catch(err => ctx.throw(500, err))
})

module.exports = router
