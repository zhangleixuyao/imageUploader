var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    path = require('path');

var UserSchema = new Schema({
  username:    { type: String },
  password:    { type: String },
  email:       { type: String },
  type:       { type: Number, 'default': 0 } //0 普通用户,1 管理员
});

mongoose.model('User', UserSchema);
